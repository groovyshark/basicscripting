﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class LinearColorChanger : MonoBehaviour
{
    [SerializeField] private float _duration;
    [SerializeField] private Color _targetColor;

    private SpriteRenderer _target;
    private Color _startColor;
    private float _runningTime;

    void Start()
    {
        _target = GetComponent<SpriteRenderer>();
        _startColor = _target.color;
    }

    void Update()
    {
        if (_runningTime <= _duration)
        {
            _runningTime += Time.deltaTime;

            float normilizedRunningTime = _runningTime / _duration;

            _target.color = Color.Lerp(_startColor, _targetColor, normilizedRunningTime);
        }
    }
}
