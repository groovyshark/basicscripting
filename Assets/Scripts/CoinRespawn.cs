﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(SpriteRenderer))]
[RequireComponent (typeof(BoxCollider2D))]
public class  CoinRespawn : MonoBehaviour
{
    [SerializeField] private float _respawnTime;

    private SpriteRenderer _renderer;
    private BoxCollider2D _collider;
    
    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();
    }

    public void Respawn()
    {
        SetCoinActive(false);
        StartCoroutine(RespawnCorutine());
    }

    private IEnumerator RespawnCorutine()
    {
        yield return new WaitForSeconds(_respawnTime);
        
        SetCoinActive(true);
    }

    private void SetCoinActive(bool active)
    {
        _renderer.enabled = active;
        _collider.enabled = active;
    }
}
