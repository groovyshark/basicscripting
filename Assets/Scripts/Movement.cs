﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Movement : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;

    private Rigidbody2D _rigidbody;
    private Animator _animator;
    private SpriteRenderer _renderer;
    

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        Move();
        Jump();
    }

    private void Move()
    {
        var direction = Vector2.right * _speed;
        if (Input.GetKey(KeyCode.D))
        {
            _rigidbody.AddForce(direction);
            _renderer.flipX = false;
        }

        if (Input.GetKey(KeyCode.A))
        {
            _rigidbody.AddForce(-direction);
            _renderer.flipX = true;
        }

        float absoluteRounded = (float)Math.Abs(Math.Round(_rigidbody.velocity.x));
        _animator.SetFloat("Speed", absoluteRounded);
    }

    private void Jump()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            _rigidbody.AddRelativeForce(Vector2.up * _jumpForce, ForceMode2D.Impulse);
        }
        
        _animator.SetFloat("JumpSpeed", _rigidbody.velocity.y);
    }
}
